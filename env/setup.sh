_rebuild_recipes_dev_root()
{
  echo "$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
  return 0
}

rebuild_recipes_dev()
{
  rebuild_dev 0
  bes_setup $(_rebuild_recipes_dev_root)
  local _root=$(_rebuild_recipes_dev_root)
  bes_env_path_remove PATH ${_root}/bin
  bes_env_path_remove PYTHONPATH ${_root}/lib
  bes_env_path_prepend REBUILD_RECIPE_PATH ${_root}
  return 0
}
