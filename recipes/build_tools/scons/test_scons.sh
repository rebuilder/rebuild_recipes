#!/bin/bash

set -e

source ${REBUILD_SHELL_FRAMEWORK_DIR}/bes_shell.sh

set -x

function test_scons()
{
  here=$(pwd)
  ( cd ${REBUILD_RECIPE_DIR} && tar cf - test_scons_project | ( cd ${here} ; tar xf - ) )
  cd test_scons_project
  scons
  expected_output="itworks"
  actual_output=$(./test_prog)
  bes_assert "[ ${expected_output} = ${actual_output} ]"
}

bes_testing_run_unit_tests
