#include <libintl.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
  const char* rv = ngettext("foo", "bar", 0);
  printf("result=%s\n", rv != NULL ? rv : "NULL");
  return 0;
}
