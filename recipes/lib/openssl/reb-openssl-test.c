#include <stdio.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/sha.h>
#include <openssl/ssl.h>

static void test_bio()
{
  BIO* bio_out = BIO_new_fp(stdout, BIO_NOCLOSE);
  BIO_printf(bio_out, "reb-openssl-test.c: test_bio()\n");
  BIO_free_all(bio_out);  
}

static void test_sha1()
{
  unsigned char ibuf[] = "reb-openssl-test.c: test_sha1()\n";
  unsigned char obuf[20];

  SHA1(ibuf, strlen(ibuf), obuf);

  int i;
  printf("reb-openssl-test.c: test_sha1() ");
  for (i = 0; i < 20; i++) {
    printf("%02x ", obuf[i]);
  }
  printf("\n");
}

int main()
{
  test_bio();
  test_sha1();
  return 0;
}
