#!/bin/bash

set -e
source ${REBUILD_SHELL_FRAMEWORK_DIR}/bes_shell.sh
set -x

echo "PATH=$PATH"
echo "PYTHONPATH=$PYTHONPATH"

aws_exe=$(which aws)
echo "aws_exe: ${aws_exe}"

aws_version=$( ${aws_exe} --version 2>&1 |awk '{ print $1; }'  | awk -F"/" '{ print $2; }' )

bes_assert "[ ${aws_version} = ${REBUILD_PACKAGE_UPSTREAM_VERSION} ]"
