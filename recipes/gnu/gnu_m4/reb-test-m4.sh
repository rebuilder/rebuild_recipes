#!/bin/bash

set -e

tmp=/tmp/reb_test$$

${M4} ${REBUILD_RECIPE_DIR}/test.m4 > ${tmp}

diff -u ${REBUILD_RECIPE_DIR}/expected_output.txt ${tmp}

rm -f ${tmp}
