#!/usr/bin/env python

import os, os.path as path, subprocess, tempfile

flex_output = tempfile.mktemp(suffix = '.yy.c')
flex_exe = os.environ['FLEX']
source_dir = os.environ['REBUILD_RECIPE_DIR']
flex_input = path.join(source_dir, 'sample1.lex')
subprocess.check_call([ flex_exe, '-o', flex_output, flex_input ])

content = open(flex_output, 'r').read()
content = content.replace(flex_input, '@FLEX_INPUT@')
content = content.replace(flex_output, '@FLEX_OUTPUT@')

flex_output_fixed = tempfile.mktemp(suffix = '.yy.c')
with open(flex_output_fixed, 'w') as f:
  f.write(content)
  f.flush()
  f.close()

flex_output_expected = path.join(source_dir, 'sample1.yy.c')
subprocess.check_call([ 'diff', '-u', flex_output_expected, flex_output_fixed ])

raise SystemExit(0)
