#!/usr/bin/env python

import os, os.path as path, subprocess, tempfile

bison_output = tempfile.mktemp(suffix = '.c')
bison_exe = os.environ['BISON']
source_dir = os.environ['REBUILD_RECIPE_DIR']
bison_input = path.join(source_dir, 'rpcalc.y')
subprocess.check_call([ bison_exe, bison_input, '-o', bison_output ])

content = open(bison_output, 'r').read()
content = content.replace(bison_input, '@BISON_INPUT@')
content = content.replace(bison_output, '@BISON_OUTPUT@')

bison_output_fixed = tempfile.mktemp(suffix = '.c')
with open(bison_output_fixed, 'w') as f:
  f.write(content)
  f.flush()
  f.close()

bison_output_expected = path.join(source_dir, 'expected_rpcalc.c')
subprocess.check_call([ 'diff', '-u', bison_output_expected, bison_output_fixed ])

raise SystemExit(0)
