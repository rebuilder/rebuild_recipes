import unittest

class python_dohq_artifactory_test(unittest.TestCase):

  def test_artifactory(self):
    from artifactory import ArtifactoryPath
    path = ArtifactoryPath("http://repo.jfrog.org/artifactory/distributions/org/")
    for p in path.glob("**/*.gz"):
      print('artifactory path: %s' % (p))

if __name__ == '__main__':
  unittest.main()
