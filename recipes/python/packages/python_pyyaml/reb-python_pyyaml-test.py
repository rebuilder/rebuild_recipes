import unittest

class reb_python_pyyaml_test(unittest.TestCase):

  def test_yaml(self):
    from yaml import load, dump
    from yaml import Loader, Dumper
    filename = "${REBUILD_RECIPE_DIR}/test.yaml"
    with open(filename, 'r') as fin:
      data = load(fin, Loader = Loader)
      output = dump(data, Dumper = Dumper)
      print(output)
    
if __name__ == '__main__':
  unittest.main()
