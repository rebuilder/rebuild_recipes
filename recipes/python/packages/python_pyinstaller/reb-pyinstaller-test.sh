#!/bin/bash

set -e

source ${REBUILD_SHELL_FRAMEWORK_DIR}/bes_shell.sh

echo PYTHONPATH=$PYTHONPATH
echo PATH=$PATH

function test_pyinstaller()
{
  cp ${REBUILD_RECIPE_DIR}/*.py  ${REBUILD_RECIPE_DIR}/test_prog.spec .
  chmod 755 test_prog.py
  local _exe=$(which pyinstaller)
  #pyinstaller --onefile --log INFO -F test_prog.py
  ${_exe} --onefile --log DEBUG --debug=all test_prog.spec

  local _expected_output="test_re:test_threading:test_subprocess:test_subprocess_with_shell:test_json_hidden:test_fakelib1:test_fakelib2_hidden:"
  local _actual_output=$(./dist/test_prog)
  echo expected_output _"${_expected_output}"_
  echo actual_output _"${_actual_output}"_
  bes_assert "[ ${_expected_output} = ${_actual_output} ]"
}

bes_testing_run_unit_tests
