#!/usr/bin/python3

import sys

def test_re():
  'Test that re works'
  import re
  C = re.compile('^foo.*$')
  sys.stdout.write('test_re:')

def test_threading():
  'Test that threading works'
  import threading
  def worker():
    sys.stdout.write('test_threading:')
    return

  t = threading.Thread(target = worker)
  t.start()
  t.join()

def test_subprocess():
  'Test that subprocess works'
  import subprocess
  p = subprocess.check_output([ 'echo', 'test_subprocess:' ])
  sys.stdout.write(p.decode('utf8').strip())
  
def test_subprocess_with_shell():
  'Test that subprocess with shell = True works'
  import subprocess
  p = subprocess.check_output('echo test_subprocess_with_shell:', shell = True)
  sys.stdout.write(p.decode('utf8').strip())

def test_json_hidden():
  'Test that using a stdlib module like json works by importing it hidden (not explicit)'
  exec('import json')
  s = json.dumps({'foo': 'hi'})
  sys.stdout.write('test_json_hidden:')

def test_fakelib1():
  'Test that we can use something in a custom import.'
  from fakelib1 import fakelib1
  sys.stdout.write(fakelib1().something)

def test_fakelib2_hidden():
  'Test that using a personal module like fakelib works by importing it hidden (not explicit)'
  exec('from fakelib2 import fakelib2')
  sys.stdout.write(fakelib2().something)

tests = [
  test_re,
  test_threading,
  test_subprocess,
  test_subprocess_with_shell,
  test_json_hidden,
  test_fakelib1,
  test_fakelib2_hidden,
]

for test in tests:
  test()

#import json
#from fakelib.foo import foo
#f = foo()
#sys.stdout.write(f.something)

sys.stdout.write('\n')
sys.stdout.flush()

raise SystemExit()
