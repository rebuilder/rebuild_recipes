import unittest

class reb_pillow_test(unittest.TestCase):

  def test_font(self):
    from cryptography.fernet import Fernet
    key = Fernet.generate_key()
    f = Fernet(key)
    token = f.encrypt(b"A really secret message. Not for prying eyes.")
    print(token)
    print(f.decrypt(token))
    
if __name__ == '__main__':
  unittest.main()
