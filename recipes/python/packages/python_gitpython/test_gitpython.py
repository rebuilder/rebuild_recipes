#!/usr/bin/env python

import unittest

class test_gitpython(unittest.TestCase):

  def test_gitpython(self):
    import os, tempfile
    from git import Repo
    rw_dir = tempfile.mkdtemp()
    bare_repo = Repo.init(os.path.join(rw_dir, 'bare-repo'), bare=True)
    assert bare_repo.bare
    
if __name__ == '__main__':
  unittest.main()
