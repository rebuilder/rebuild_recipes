import unittest

class test_ruamel_yaml(unittest.TestCase):

  def test_yaml(self):
    from ruamel.yaml import load, dump
    from ruamel.yaml import Loader, Dumper
    filename = "${REBUILD_RECIPE_DIR}/test.yaml"
    with open(filename, 'r') as fin:
      data = load(fin, Loader = Loader)
      output = dump(data, Dumper = Dumper)
      print(output)
    
if __name__ == '__main__':
  unittest.main()
