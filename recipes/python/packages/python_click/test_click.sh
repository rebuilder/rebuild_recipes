#!/bin/bash

set -e

source ${REBUILD_SHELL_FRAMEWORK_DIR}/bes_shell.sh

set -x

function test_click_prog()
{
  local _click_prog=${REBUILD_RECIPE_DIR}/click_prog.py
  bes_assert "[ $(${_click_prog} --op + 1 2) = 3 ]"
  bes_assert "[ $(${_click_prog} --op - 4 3) = 1 ]"
}

bes_testing_run_unit_tests
