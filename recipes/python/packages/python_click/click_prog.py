#!/usr/bin/env python

import click

@click.command()
@click.option('--op', default = '+', help = 'Operator')
@click.argument('n1', type = int)
@click.argument('n2', type = int)
def hello(op, n1, n2):
  if op == '+':
    result = n1 + n2
  elif op == '-':
    result = n1 - n2
  print(result)
  
if __name__ == '__main__':
  hello()
