import unittest

class test_python_requests(unittest.TestCase):

  def test_ssl(self):
    import requests
    r = requests.get('https://github.com', verify = True)
    self.assertEqual( 200, r.status_code )

if __name__ == '__main__':
  unittest.main()
