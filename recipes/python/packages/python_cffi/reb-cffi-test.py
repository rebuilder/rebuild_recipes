import unittest

class reb_cffi_test(unittest.TestCase):

  def test_pylms(self):
    from cffi import FFI
    ffi = FFI()
    ffi.cdef("""
int printf(const char *format, ...);   // copy-pasted from the man page
    """)
    C = ffi.dlopen(None)                     # loads the entire C namespace
    arg = ffi.new("char[]", b"world")         # equivalent to C code: char arg[] = "world";
    C.printf(b"hi there, %s.\n", arg)         # call printf
    
if __name__ == '__main__':
  unittest.main()
