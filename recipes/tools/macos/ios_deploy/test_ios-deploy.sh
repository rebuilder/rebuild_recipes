#!/bin/bash

set -e

source ${REBUILD_SHELL_FRAMEWORK_DIR}/bes_shell.sh

set -x

function test_version()
{
  local _exe=$(which ios-deploy)
  local _version=$(${_exe} -V)
  bes_assert "[ ${_version} = ${REBUILD_PACKAGE_FULL_VERSION} ]"
}

bes_testing_run_unit_tests
