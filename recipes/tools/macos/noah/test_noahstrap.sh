#!/bin/bash

set -e

source ${REBUILD_SHELL_FRAMEWORK_DIR}/bes_shell.sh

set -x

function test_version()
{
  local _exe=$(which noahstrap)
  local _version=$(${_exe} --version)
  bes_assert "[ ${_version} = ${REBUILD_PACKAGE_FULL_VERSION} ]"
}

bes_testing_run_unit_tests
