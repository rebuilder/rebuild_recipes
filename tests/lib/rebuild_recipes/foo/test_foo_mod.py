#!/usr/bin/env python
#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

import os.path as path, unittest

from rebuild_recipes.foo import foo_mod

class test_foo_mod(unittest.TestCase):

  def test_foo_func(self):
    self.assertEqual( 2, foo_mod.foo_func(1) )

if __name__ == "__main__":
  unittest.main()
