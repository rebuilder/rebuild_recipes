#!/bin/bash

set -e
set -x

PREFIX=${PREFIX:-/usr/local}

dest=${PREFIX}/share/rebuild

mkdir -p ${dest}

tar cf - recipes *.reproject | ( cd ${dest} ; tar xf - )
