#!/bin/bash
#export BES_LOG='requirement_manager=debug format=brief'
exec rebuilder.py -w \
  --storage-config config/storage.config \
  --sources-config-name local_sources \
  -f rebuild_base_recipes.reproject \
  --checksum-cache=~/.rebuild_checksums.db ${1+"$@"}
