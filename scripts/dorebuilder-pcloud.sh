#!/bin/bash
#export BES_LOG='requirement_manager=debug format=brief'
exec rem_rebuilder -w \
  --storage-config config/storage.config \
  --sources-config-name pcloud_ramiro \
  -f rebuild_base_recipes.reproject \
  --checksum-cache=~/.rebuild_checksums.db ${1+"$@"}
