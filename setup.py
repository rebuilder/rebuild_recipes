#!/usr/bin/env python
#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

from os import path
from setuptools import setup, find_packages
from bes.fs.file_find import file_find
from bes.fs.file_path import file_path
from bes.fs.file_util import file_util
from bes.fs.file_copy import file_copy

# Copy all the reproject and recipes into lib/rebuild_recipes/project
# so it can be part of the egg
this_dir = path.dirname(__file__)
src_dir = path.join(this_dir, 'recipes')
dst_dir = path.join(this_dir, 'lib/rebuild_recipes/project/recipes')
recipe_files = file_find.find(src_dir, relative = True, file_type = file_find.FILE)

from bes.system.log import log
log.output('file_copy.copy_tree(%s, %s)\n' % (src_dir, dst_dir), console = True)

file_copy.copy_tree(src_dir, dst_dir)

project_files = file_path.glob(this_dir, '*.reproject')
for f in project_files:
  file_util.copy(f, path.join('lib/rebuild_recipes/project', f))

data = [ 'project/recipes/%s' % (f) for f in recipe_files ]

project_file_data = [ 'project/%s' % (f) for f in project_files ]

data.extend(project_file_data)

ver = {}
exec(open('lib/rebuild_recipes/ver.py', 'r').read(), {}, ver)
setup(
  name = 'rebuild_recipes',
  version = ver['BES_VERSION'],
  packages = find_packages('lib'),
  package_dir= {'' : 'lib'},
  package_data = { 'rebuild_recipes': data },
  zip_safe = True,
  author = ver['BES_AUTHOR_NAME'],
  author_email = ver['BES_AUTHOR_EMAIL'],
  install_requires = [ 'bes', 'rebuild' ],
)
