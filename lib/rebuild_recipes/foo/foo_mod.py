#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

class foo_mod(object):
  'foo_mod'

  @classmethod
  def foo_func(clazz, x):
    return x + 1
